#ifndef SPARSE_MATCHER_H
#define SPARSE_MATCHER_H

#include "parse_mgf.hpp"
#include "spectra_mgf.hpp"

#include "matcher.hpp"
#include <vector>
#include <unordered_map>
#include <cmath>
#include <iostream>
#include <cassert>
#include <chrono>

class SparseMatcher : public Matcher 
{
public:
    SparseMatcher(double match_threshold);

    void match(ParseMGF& parseMGF, std::vector<std::pair<int, int>>& matches);

    double match_threshold; // Threshold for dot products to be considered a match
private:
    std::vector<std::vector<std::unordered_map<int, double>>> matrix;

    void matchSpectra(int index, ParseMGF& parseMGF, std::vector<std::pair<int, int>>& matches); 
    
    void constructMatrix(ParseMGF& parseMGF); 
};

#endif
