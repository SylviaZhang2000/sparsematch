#include "bruteforce_matcher.hpp"

BruteforceMatcher::BruteforceMatcher(double match_threshold){
    this->match_threshold = match_threshold; 
    return;
}

void BruteforceMatcher::match(ParseMGF& parseMGF, std::vector<std::pair<int,int>>& matches) {
    for(int i = 0; i < parseMGF.spectras.size(); i++){
        for(int j = 0; j < parseMGF.spectras.size(); j++){
            //If the rounded masses match - calculate dot product, same as MSCluster 
            if(parseMGF.spectras[i].mass_rounded == parseMGF.spectras[j].mass_rounded){
                double dot_product = dotProduct(parseMGF.spectras[i], parseMGF.spectras[j]);
                if(dot_product > match_threshold){
                    //std::cout << "Found match: " << parseMGF.spectras[i].id << ", " << parseMGF.spectras[j].id << " with dot prod: " << dot_product << std::endl;
                    matches.push_back(std::make_pair(parseMGF.spectras[i].id, parseMGF.spectras[j].id));
                }                  
            }
        }
    }
    return;
}



double BruteforceMatcher::dotProduct(SpectraMGF& s1, SpectraMGF& s2) {
    //First put all s2 peaks in a hash table:
    std::unordered_map<int, double> s2_peaks;
    for(int i = 0; i < s2.length; i++){
        int pos = s2.peak_positions_rounded[i];
        double mag = s2.peak_magnitudes_normalized[i];
        s2_peaks.insert(std::make_pair(pos, mag));
    }
    //Now compute dot product:    
    double result = 0.0;
    for(int i = 0; i < s1.length; i++){
        int pos = s1.peak_positions_rounded[i];
        if(s2_peaks.find(pos) != s2_peaks.end()){
            double s1_mag = s1.peak_magnitudes_normalized[i];
            double s2_mag = s2_peaks[pos];
            result += s1_mag * s2_mag;
        }
    }
    return result;
}
