#ifndef BRUTEFORCE_MATCHER_H
#define BRUTEFORCE_MATCHER_H

#include "parse_mgf.hpp"
#include "spectra_mgf.hpp"

#include "matcher.hpp"
#include <vector>
#include <unordered_map>
#include <cmath>
#include <iostream>
#include <cassert>


class BruteforceMatcher : public Matcher 
{
public:
    BruteforceMatcher(double match_threshold);

    void match(ParseMGF& parseMGF, std::vector<std::pair<int, int>>& matches);

    double match_threshold; // Threshold for dot products to be considered a match
private:
    double dotProduct(SpectraMGF& s1, SpectraMGF& s2); 
    
};

#endif 
